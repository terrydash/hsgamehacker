from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/permission/auth/login', methods=["get", "post"])
def hello_world():
    r = {"result": True, "errorCode": 0, "msg": "",
         "json": {"menusTree": [], "menus": [], "accessToken": "5c4d88917a34ede8e6711f8c60d81aec",
                  "user": {"id": "6ba8c70da65c498ba814a99210186d30", "creatorId": None, "creatorName": None,
                           "createTime": "2019-10-25 18:56:32", "modifierId": None, "modifierName": None,
                           "modifyTime": "2020-03-29 22:19:00", "deleted": 0, "username": "MLJ6Y5BTWPNT",
                           "nickname": None, "mobile": None, "orgId": None, "headUrl": None, "sex": None,
                           "password": "15ba9ec2c0ba3c54a57633db3c29be32", "payPassword": None, "birthday": None,
                           "type": None, "isAdmin": 0, "isFront": 1, "status": 0, "isTry": None, "identity": None,
                           "isUsed": 0, "lastCheckTime": "2020-03-29T14:19:00.000+0000"}}, "redirectUrl": ""}

    return jsonify(r)


@app.route("/hsgame/game/getById", methods=["get", "post"])
def getgameid():
    r = {"result": True, "errorCode": 0, "msg": "",
         "json": {"id": "41a64ccbf7dd654333b34ee6906cd014", "deleted": 0, "creatorId": None, "creatorName": None,
                  "createTime": "2019-08-07 09:56:32", "modifierId": None, "modifierName": None,
                  "modifyTime": "2020-03-30 14:14:33", "name": "死亡细胞1111", "pinyin": "siwangxibao",
                  "pinyinFirst": "SWXB",
                  "newGame": 0, "activeCount": 0, "playCount": 0, "encodeKey": "83NjTSqdjsuL7CTPlQYFchvAEscGaBW8",
                  "sale": 1, "timeLimit": 0, "timeLimitNotice": "",
                  "headUrl": "/upload/game/head/202003301414310118.jpg",
                  "videoUrl": "https://cloud.video.taobao.com//play/u/49123702/p/1/e/6/t/1/233593061610.mp4",
                  "giftUrl": "https://pan.baidu.com/s/1gVWzBMhaPzr_Xh3OjiyjgQ",
                  "downloadUrl": "https://pan.baidu.com/s/1OHjtAJp_PPYbA3Ho4vyhvA", "downloadPassword": "",
                  "buyUrl": "https://detail.tmall.com/item.htm?spm=a212k0.12153887.0.0.73b5687dSs5dIu&id=600558144970&sku_properties=122216883:27889",
                  "readmeUrl": "failed to load library sdl.hdll 到根目录打开deadcells.exe即可运行游戏", "gameDetailImages": [
                 {"id": "e600a94556de4ef785018f65729a2274", "gameId": "41a64ccbf7dd654333b34ee6906cd014", "orderNo": 1,
                  "imgUrl": "https://img.alicdn.com/imgextra/i3/49123702/O1CN01kIBN9c1dDYn2UpmVa_!!49123702.jpg",
                  "imgData": None}]}, "redirectUrl": ""}
    return jsonify(r)


@app.route('/upload/autoupdate/client/UpdateConfiguration.json', methods=["get", "post"])
def uploadchecj():
    r = {
        "ClientVersion": 28,
        "IsMust": True,
        "files": [
            {
                "FileRelativePath": "AxInterop.WMPLib.dll",
                "FileSize": 53760,
                "LastUpdateTime": "2019-08-13T12:39:40",
                "Version": 1.0
            },
            {
                "FileRelativePath": "EXLAUS.Core.dll",
                "FileSize": 11264,
                "LastUpdateTime": "2019-08-11T05:09:54",
                "Version": 1.0
            },
            {
                "FileRelativePath": "hsgame.exe",
                "FileSize": 7680,
                "LastUpdateTime": "2019-08-15T22:43:14",
                "Version": 2.0
            },
            {
                "FileRelativePath": "hsgame_client.dll",
                "FileSize": 5959680,
                "LastUpdateTime": "2019-11-18T07:39:12",
                "Version": 18.0
            },
            {
                "FileRelativePath": "hsgame_client.dll.config",
                "FileSize": 569,
                "LastUpdateTime": "2019-08-16T07:14:47",
                "Version": 2.0
            },
            {
                "FileRelativePath": "hsgame_core.dll",
                "FileSize": 49664,
                "LastUpdateTime": "2019-11-18T07:39:04",
                "Version": 10.0
            },
            {
                "FileRelativePath": "hsgame_query.exe",
                "FileSize": 1285120,
                "LastUpdateTime": "2019-11-19T08:14:58.7472636+08:00",
                "Version": 20.0
            },
            {
                "FileRelativePath": "icon.ico",
                "FileSize": 67646,
                "LastUpdateTime": "2019-08-13T18:35:57",
                "Version": 2.0
            },
            {
                "FileRelativePath": "Interop.WMPLib.dll",
                "FileSize": 330752,
                "LastUpdateTime": "2019-08-13T12:39:39",
                "Version": 1.0
            },
            {
                "FileRelativePath": "Newtonsoft.Json.dll",
                "FileSize": 525824,
                "LastUpdateTime": "2017-06-18T13:57:10",
                "Version": 1.0
            },
            {
                "FileRelativePath": "RestSharp.dll",
                "FileSize": 168960,
                "LastUpdateTime": "2015-08-26T16:33:34",
                "Version": 1.0
            }
        ]
    }
    return jsonify(r)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
